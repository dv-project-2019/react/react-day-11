import React from 'react';
import { PageHeader } from 'antd';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { onClickBack } from '../../actions/authActions';
import { useHistory } from 'react-router';

const Header = () => {
    
    const history = useHistory();

    return (
        <div style={{
            backgroundColor: '#007acc'
        }}>
            <PageHeader
                onBack={() => history.goBack()}
                title="Back"
            />
        </div>

    )
}

export default Header;