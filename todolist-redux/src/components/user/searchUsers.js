import React from 'react';
import { Input } from 'antd';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { searchUserName } from '../../actions/todoActions';

const SearchUsers = (props) => {

    const { Search } = Input;

    const { searchUserName } = props;

    const searchUserByName = (event) => {
        searchUserName(event.target.value);
    }

    return (
        <Search
            placeholder="input user name"
            onChange={searchUserByName}
            style={{ width: 300, height: 40 }}
        />
    );
}

const mapDispatchToprops = (dispatch) => {
    return bindActionCreators({ searchUserName }, dispatch);
}

export default connect(null, mapDispatchToprops)(SearchUsers);