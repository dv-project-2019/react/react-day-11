import React from 'react';
import { List, Avatar, Skeleton } from 'antd';
import { connect } from 'react-redux';
import { useHistory } from 'react-router';

const UsersList = (props) => {
    const { users, searchValue, loading } = props;
    const history = useHistory();

    return (
        <List
            className="demo-loadmore-list"
            itemLayout="horizontal"
            loading={loading}
            dataSource={
                searchValue ?
                    users.filter(user => user.name.toUpperCase().match(searchValue.toUpperCase()))
                    :
                    users
            }
            renderItem={item => (
                <List.Item
                    actions={[<a onClick={() =>  history.push("/users/" + item.id + "/todo")} >Todo</a>,
                    <a onClick={() =>  history.push("/users/" + item.id + "/albums")}>Albums</a>]}
                >
                    <Skeleton avatar title={false} loading={item.loading} active>
                        <List.Item.Meta
                            avatar={
                                <Avatar src="https://www.bsglobaltrade.com/wp-content/uploads/2016/09/photo.png" />
                            }
                            title={<a href="https://ant.design">{item.name}</a>}
                            description={item.email}
                        />
                    </Skeleton>
                </List.Item>
            )}
        />
    );
}

const mapStateToprops = (state) => {
    return {
        users: state.users.users,
        searchValue: state.users.searchValue,
        loading: state.users.loading
    }
}

export default connect(mapStateToprops, null)(UsersList);