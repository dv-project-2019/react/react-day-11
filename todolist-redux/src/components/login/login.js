import React from 'react';
import { Row, Col } from 'antd';
import { Input } from 'antd';
import './login-style.css';
import { Button } from 'antd';
import { useHistory } from 'react-router';

const Login = () => {
    const history = useHistory();

    return (
        <div className="login-page">
            <Row type="flex" justify="center" gutter={[48, 48]}>
                <Col span={12} offset={12}>
                    <h2>Login</h2>
                </Col>
            </Row>
            <Row type="flex" justify="center" gutter={[48, 48]}>
                <Col span={3}>
                    <h3>Username</h3>
                </Col>
                <Col span={6}>
                    <Input placeholder="username" />
                </Col>
            </Row>

            <Row type="flex" justify="center" gutter={[48, 48]}>
                <Col span={3}>
                    <h3>password</h3>
                </Col>
                <Col span={6}>
                    <Input placeholder="password" />
                </Col>
            </Row>

            <Row type="flex" justify="center" gutter={[48, 48]}>
                <Col span={12} offset={12}>
                    <Button type="primary" onClick={() => {
                        history.push("/processLogin")
                    }}>Login</Button>
                </Col>
            </Row>
        </div>

    )
}

export default Login;