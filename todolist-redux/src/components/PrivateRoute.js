import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const PrivateRoute = ({ path, component: Component, exact, isLogin}) => (
    <Route path={path} exact={exact} render={(props) => {
        
        if (isLogin == true) {
            return <Component {...props} />
        } else {
            return <Redirect to="/login" />
        }
    }} />
)

const mapStateToProps = (state) => {
    return{
        isLogin: state.authService.isLogin
    }
}

export default connect(mapStateToProps, null) (PrivateRoute);
