import React from 'react';
import { Button } from 'antd';
import { List, Typography } from 'antd';
import { useSelector } from 'react-redux';
import { clickDone } from '../../actions/todoActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

const TodoList = (props) => {
    
    const { todoList, selectedFilter, loading, searchKeyword, clickDone } = props;

    return (
        <List
            header={
                <p><b>Todo List</b></p>
            }

            loading={loading}
            dataSource={
                selectedFilter == "All" && searchKeyword == "" ?
                    todoList
                    :
                    selectedFilter == "All" ? 
                        todoList.filter((todoList) => todoList.title.toLowerCase().match(searchKeyword.toLowerCase()))
                        :
                        todoList.filter(todoList => todoList.completed.toString() == selectedFilter && todoList.title.toLowerCase().match(searchKeyword.toLowerCase()))
            }
            renderItem={(item, index) => (
                <List.Item>
                    {item.completed == true ?
                        <div>
                            <s> Done </s>
                            {item.title}
                        </div>

                        :
                        <div>
                            <Typography.Text mark> Doing </Typography.Text>
                            {item.title}
                        </div>
                    }
                    {item.completed == true ?
                        <p></p>
                        :
                        <Button onClick={() => clickDone(item.id)} value={index} >Done</Button>
                    }
                </List.Item>
            )}
        />
    )
}

const mapStateToprops = (state) => {
    return {
        todoList: state.todoList.todoList,
        selectedFilter: state.todoList.selectedValue,
        searchKeyword: state.todoList.searchKeyword,
        loading: state.todoList.loading
    }
}

const mapDispatchToprops = (dispatch) => {
    return bindActionCreators({ clickDone }, dispatch)
}

export default connect(mapStateToprops, mapDispatchToprops) (TodoList);