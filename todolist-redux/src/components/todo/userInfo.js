import React from 'react';
import { Descriptions } from 'antd';
import { connect } from 'react-redux';

const UserInfo = (props) => {

    const { userInfo }  = props;

    return (
        <div className="user-info">
            <Descriptions title={"User : " + userInfo.name} bordered>
                <Descriptions.Item label="UserName">{userInfo.username}</Descriptions.Item>
                <Descriptions.Item label="Telephone">{userInfo.phone}</Descriptions.Item>
                <Descriptions.Item label="Website">{userInfo.website}</Descriptions.Item>
                <Descriptions.Item label="Email">{userInfo.email}</Descriptions.Item>
            </Descriptions>
        </div>
    )
}

const mapStateToprops = (state) => {
    return {
        userInfo: state.users.userInfo
    }
}

export default connect(mapStateToprops, null) (UserInfo);
