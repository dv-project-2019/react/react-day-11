import React from 'react';
import { Select } from 'antd';
import { connect } from 'react-redux';
import { setSelectedValue } from '../../actions/todoActions';
import { bindActionCreators } from 'redux';

const TodoFilter = (props) => {
    const { Option } = Select;

    const { selectedValue, setSelectedValue } = props;

    return (
        <div className="select-box">
            <Select value={selectedValue} onChange={(value) => setSelectedValue(value)} className="select">
                <Option value="All">All</Option>
                <Option value="false">Doing</Option>
                <Option value="true">Done</Option>
            </Select>
        </div>

    )
}

const mapStateToprops = (state) => {
    return {
        selectedValue: state.todoList.selectedValue
    }
}

const mapDispatchToprops = (dispatch) => {
    return bindActionCreators({ setSelectedValue }, dispatch);
}

export default connect(mapStateToprops, mapDispatchToprops) (TodoFilter);