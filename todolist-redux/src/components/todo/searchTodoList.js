import React from 'react';
import { searchTodoByKeyword } from '../../actions/todoActions';
import { Input } from 'antd';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

const SearchTodoList = (props) => {
    const { Search } = Input;

    const { searchTodoByKeyword } = props;

    const onSearchKeyword = (event) => {
        searchTodoByKeyword(event.target.value);
    };

    return (
        <div className="select-box">
            <Search
                placeholder="input search text"
                onChange={onSearchKeyword}
                style={{ width: 200 }}
            />
        </div>
    )
}

const mapDispatchToprops = (dispatch) => {
    return bindActionCreators({ searchTodoByKeyword }, dispatch)
}

export default connect(null, mapDispatchToprops)(SearchTodoList);