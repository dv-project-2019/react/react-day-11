import React, { useState, useEffect } from 'react';
import { Layout, } from 'antd';
import './todo-style.css';
import UserInfo from '../../components/todo/userInfo';
import TodoFilter from '../../components/todo/todoFilter';
import TodoList from '../../components/todo/todoList';
import { connect, useDispatch } from 'react-redux';
import { fetchUserInfo, fetchTodos } from '../../actions/todoActions';
import SearchTodoList from '../../components/todo/searchTodoList';
import { bindActionCreators } from 'redux';
import Header from '../../components/header/header';

const TodoPage = (props) => {
    const { Content, Footer } = Layout;

    const userId = props.match.params.user_id;

    const { fetchUserInfo, fetchTodos } = props;

    useEffect(() => {
        fetchUserInfo(userId);
        fetchTodos(userId);
    }, []);

    return (
        <Layout className="layout">
            <Header />
            <Content className="content">
                <div className="topic">
                    <p className="todolist-text"><b>Todo List</b></p>
                </div>
                <div className="main-todo-page">
                    <UserInfo />
                    <SearchTodoList />
                    <TodoFilter />
                    <TodoList />
                </div>
            </Content>
            <Footer style={{ textAlign: 'center' }}>Copy Right ©2020 Created by Waan Thipnirun</Footer>
        </Layout>
    )
}

const mapDispatchToprops = (dispatch) => {
    return bindActionCreators({ fetchUserInfo, fetchTodos }, dispatch)
}

export default connect(null, mapDispatchToprops)(TodoPage);