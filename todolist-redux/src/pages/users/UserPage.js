import React, { useEffect } from 'react';
import { Layout } from 'antd';
import './userpage-style.css';
import SearchUsers from '../../components/user/searchUsers';
import UsersList from '../../components/user/usersList';
import { fetchUsers } from '../../actions/todoActions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Row, Col } from 'antd';
import { Button } from 'antd';
import { useHistory } from 'react-router';

const UserPage = (props) => {
    const { fetchUsers } = props;
    const { Header, Content, Footer } = Layout;
    const history = useHistory();

    useEffect(() => {
        fetchUsers()
    }, []);

    return (
        <Layout className="layout">
            <Header style={{
                backgroundColor: '#007acc'
            }}>
                <Row>
                    <Col span={11} offset={6}>
                        <h2 className="userlist-text">Users List</h2>
                    </Col>
                    <Col span={1} offset={5}>
                        <div className="logout-button">
                            <Button type="danger" onClick={() => {
                                history.push("/processLogout")
                            }}>Logout</Button>
                        </div>
                    </Col>
                </Row>
            </Header>

            <Content class="content">
                <div style={{ margin: '16px 0' }}>
                    <SearchUsers />
                </div>
                <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>
                    <UsersList />
                </div>
            </Content>
            <Footer style={{ textAlign: 'center' }}>Copy Right ©2020 Created by Waan Thipnirun</Footer>
        </Layout>
    );
}

const mapDispatchToprops = (dispatch) => {

    return bindActionCreators({ fetchUsers }, dispatch)
}

export default connect(null, mapDispatchToprops)(UserPage);

// export default UserPage;