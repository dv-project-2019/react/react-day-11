import React, { useState, useEffect } from 'react';
import { Layout, } from 'antd';
import { Card } from 'antd';
import { List, Avatar, Icon } from 'antd';
import { PageHeader } from 'antd';
import './pictureList-style.css';

const PicturePage = (props) => {
    const [pictures, setPictures] = useState([]);
    const { Header, Content, Footer } = Layout;

    const { Meta } = Card;
    const albumId = props.match.params.album_id;

    useEffect(() => {
        fetchAlbums();
    }, []);

    const fetchAlbums = () => {
        fetch('https://jsonplaceholder.typicode.com/photos?albumId=' + albumId)
            .then(response => response.json())
            .then(data => {
                setPictures(data);
            })
            .catch(error => console.log(error));
    }

    const IconText = ({ type, text }) => (
        <span>
            <Icon type={type} style={{ marginRight: 8 }} />
            {text}
        </span>
    );

    return (
        <Layout className="layout">
            <PageHeader
                style={{
                    border: '1px solid rgb(235, 237, 240)',
                }}
                onBack={() => window.history.back()}
                title="back" />
            <Content style={{ padding: '0 50px' }}>
                <div class="topic">
                    <h2 className="picturelist-text">{"Pictures Lists of Album No." + albumId}</h2>
                </div>
                <div className="pictureList">
                    <List
                        itemLayout="vertical"
                        size="large"
                        pagination={{
                            onChange: page => {
                                console.log(page);
                            },
                            pageSize: 3,
                        }}
                        dataSource={pictures}
                        renderItem={(picture, index) => (
                            <List.Item
                                key={picture.title}
                                extra={
                                    <img
                                        width={272}
                                        alt="logo"
                                        src={picture.url}
                                    />
                                }
                            >
                                <List.Item.Meta
                                    title={<a href={picture.url}>
                                        <b>No.{index+1}</b> {picture.title}</a>}
                                    description={picture.thumbnailUrl}
                                />
                            </List.Item>
                        )}
                    />
                </div>
            </Content>
            <Footer style={{ textAlign: 'center' }}>Copy Right ©2020 Created by Waan Thipnirun</Footer>
        </Layout>
    )
}
export default PicturePage;
