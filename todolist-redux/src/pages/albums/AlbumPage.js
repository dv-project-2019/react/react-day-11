import React, { useState, useEffect } from 'react';
import { Layout, } from 'antd';
import { List, Card } from 'antd';
import { Avatar } from 'antd';
import './album-style.css';

const { Header, Content, Footer } = Layout;

const AlbumPage = (props) => {

    const [albums, setAlbums] = useState([]);

    const userId = props.match.params.user_id;

    useEffect(() => {
        fetchAlbums();
    }, []);

    const fetchAlbums = () => {
        //get user id
        fetch('https://jsonplaceholder.typicode.com/albums?userId=' + userId)
            .then(response => response.json())
            .then(data => {
                setAlbums(data);
            })
            .catch(error => console.log(error));
    }

    console.log(albums)
    return (
        <Layout className="layout">
            <Header>
                <h2 className="albumlist-text">Lists of albums</h2>
            </Header>
            <Content style={{ padding: '0 50px' }}>
                <div style={{ margin: '16px 0' }}>
                    <List
                        grid={{
                            gutter: 16,
                        }}
                        dataSource={albums}
                        renderItem={(album, index) => (
                            <List.Item>
                                <Card title={"Album No." + (index + 1)}>
                                    <Avatar
                                        shape="square"
                                        size={100}
                                        src={"https://www.weavers.space/rw_common/plugins/stacks/dynamics/api.php/imageworks/products/image-gallery/icon/icon.png?w=500&h=500&q=85&dpr=1&format=png&date=2018-08-02T08:59:28+00:00"}
                                    />
                                    <a className="albumName" href={"/users/" + userId + "/albums/" + album.id}>
                                        <b>Album name:</b> {album.title}
                                    </a>
                                </Card>
                            </List.Item>
                        )}
                    /></div>
            </Content>
            <Footer style={{ textAlign: 'center' }}>Copy Right ©2020 Created by Waan Thipnirun</Footer>
        </Layout>
    )
}
export default AlbumPage;