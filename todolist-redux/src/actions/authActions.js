export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
export const CLICK_LOGIN = 'CLICK_LOGIN';
export const CLICK_LOGOUT = 'CLICK_LOGOUT';
export const CLICK_BACK = 'CLICK_BACK';

export const login = () => {
    return {
        type: LOGIN
    }

}

export const logout = () => {
    return {
        type: LOGOUT
    }

}