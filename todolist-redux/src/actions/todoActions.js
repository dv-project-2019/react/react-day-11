export const SELECT_TODO_FILTER = 'SELECT_TODO_FILTER';
export const CLICK_DONE = 'CLICK_DONE';
export const SEARCH_TODO = 'SEARCH_TODO';
export const FETCH_USER_BEGIN = 'FETCH_USER_BEGIN';
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS';
export const FETCH_USER_ERROR = 'FETCH_USER_ERROR';
export const FETCH_TODO_BEGIN = 'FETCH_TODO_BEGIN';
export const FETCH_TODO_SUCCESS = 'FETCH_TODO_SUCCESS';
export const FETCH_TODO_ERROR = 'FETCH_TODO_ERROR';
export const FETCH_USERS_BEGIN = 'FETCH_USERS_BEGIN';
export const FETCH_USERS_SUCCESS = 'FETCH_USERS_SUCCESS';
export const FETCH_USERS_ERROR = 'FETCH_USERS_ERROR';
export const SEARCH_USER = 'SEARCH_USER';

export const fetchUsers = () => {
    return dispatch => {
        dispatch(fetchUsersBegin())
        return fetch('https://jsonplaceholder.typicode.com/users')
            .then(res => res.json())
            .then(data => {
                dispatch(fetchUsersSuccess(data))
            })
            .catch(error => dispatch(fetchUsersError(error)));
    }
}

export const fetchUserInfo = (userId) => {
    return dispatch => {
        dispatch(fetchUserBegin())
        return fetch('https://jsonplaceholder.typicode.com/users/' + userId)
            .then(res => res.json())
            .then(data => {
                dispatch(fetchUserSuccess(data))
            })
            .catch(error => dispatch(fetchUserError(error)));
    }
}

export const fetchTodos = (userId) => {
    return dispatch => {
        dispatch(fetchTodoBegin())
        return fetch('https://jsonplaceholder.typicode.com/todos?userId=' + userId)
            .then(response => response.json())
            .then(data => {
                dispatch(fetchTodoSuccess(data));
            })
            .catch(error => dispatch(fetchTodoError(error)));
    }
}

export const fetchUsersBegin = () => {
    return {
        type: FETCH_USERS_BEGIN
    }
}

export const fetchUsersSuccess = (users) => {
    return {
        type: FETCH_USERS_SUCCESS,
        users
    }
}

export const fetchUsersError = (error) => {
    return {
        type: FETCH_USERS_ERROR,
        error
    }
}

export const fetchUserBegin = () => {
    return {
        type: FETCH_USER_BEGIN
    }
}

export const fetchUserSuccess = (userInfo) => {
    return {
        type: FETCH_USER_SUCCESS,
        userInfo
    }
}

export const fetchUserError = (error) => {
    return {
        type: FETCH_USER_ERROR,
        error
    }
}

export const fetchTodoBegin = () => {
    return {
        type: FETCH_TODO_BEGIN
    }

}

export const fetchTodoSuccess = (todos) => {
    return {
        type: FETCH_TODO_SUCCESS,
        todos
    }

}

export const fetchTodoError = (error) => {
    return {
        type: FETCH_TODO_ERROR,
        error
    }

}

export const setSelectedValue = (selectValue) => {
    return {
        type: SELECT_TODO_FILTER,
        selectValue
    }    
       
}

export const clickDone = (todoId) => {
    return {
        type: CLICK_DONE,
        todoId
    }
}

export const searchTodoByKeyword = (todoName) => {
    return {
        type: SEARCH_TODO,
        todoName
    }
}

export const searchUserName = (userName) => {
    return {
        type: SEARCH_USER,
        userName
    }
}
