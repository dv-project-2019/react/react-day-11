import React from 'react';
import UserPage from './pages/users/UserPage';
import TodoPage from './pages/todo/TodoPage';
import AlbumPage from './pages/albums/AlbumPage';
import PicturePage from './pages/picture/PicturePage';
import { BrowserRouter, Route, Redirect } from 'react-router-dom';
import Login from './components/login/login';
import PrivateRoute from './components/PrivateRoute';
import Header from './components/header/header';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { login, logout } from './actions/authActions';

const Routing = (props) => {

    const { login, logout, isLogin } = props;

    return (
        <BrowserRouter>
            <Route path="/processLogin" render={() => {
                login()
                return <Redirect to="/users"></Redirect>
            }} />

            <Route path="/processLogout" render={() => {
                logout()
                return <Redirect to="/login"></Redirect>
            }} />

            <Route path="/" component={Login} exact={true} />
            <PrivateRoute path="/users" component={UserPage} exact={true} isLogin={isLogin} />
            <PrivateRoute path="/users/:user_id/todo" component={TodoPage} exact={true} isLogin={isLogin} />
            <PrivateRoute path="/users/:user_id/albums" component={AlbumPage} exact={true}  isLogin={isLogin}/>
            <PrivateRoute path="/users/:user_id/albums/:album_id" component={PicturePage} isLogin={isLogin} />
            <Route path="/login" component={Login}></Route>
            <PrivateRoute path="/header" component={Header} isLogin={isLogin}></PrivateRoute>
        </BrowserRouter>
    )
}

const mapStateToProps = (state) => {
    return {
        isLogin: state.authService.isLogin
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ login, logout }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Routing);