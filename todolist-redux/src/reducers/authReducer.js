import { LOGIN, LOGOUT, CLICK_LOGIN, CLICK_LOGOUT, CLICK_BACK } from "../actions/authActions";

const initialState = {
    isLogin: false
};

export const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN:
            return {
                ...state,
                isLogin: true
            }
        case LOGOUT:
            return {
                ...state,
                isLogin: false
            }
        default:
            return state;
    }
}
