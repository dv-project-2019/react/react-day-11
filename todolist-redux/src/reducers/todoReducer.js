import { FETCH_TODO_BEGIN, FETCH_TODO_SUCCESS, FETCH_TODO_ERROR, SELECT_TODO_FILTER, CLICK_DONE, SEARCH_TODO } from '../actions/todoActions';

const initialState = {
    todoList: [],
    selectedValue: 'All',
    searchKeyword: '',
    loading: false,
    error: ''
}

export const todoReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_TODO_BEGIN:
            return {
                ...state,
                loading: true
            };

        case FETCH_TODO_SUCCESS:
            return {
                ...state,
                todoList: action.todos,
                selectedValue: 'All',
                loading: false,
                error: ''
            };

        case FETCH_TODO_ERROR:
            return {
                ...state,
                loading: false,
                error: action.error
            };

        case SELECT_TODO_FILTER:
            return {
                ...state,
                selectedValue: action.selectValue
            }

        case CLICK_DONE:
            const todoUpdate = [...state.todoList];
            todoUpdate.map((todo) => {
                if (todo.id == action.todoId) {
                    console.log(todo.id)
                    todo.completed = true;
                }
            })
            return {
                ...state,
                todoList: todoUpdate
            }

        case SEARCH_TODO:
            return {
                ...state,
                searchKeyword: action.todoName
            }

        default:
            return state
    }
}