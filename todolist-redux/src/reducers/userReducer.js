import { FETCH_USER_BEGIN, FETCH_USER_SUCCESS, FETCH_USER_ERROR, FETCH_USERS_BEGIN, FETCH_USERS_SUCCESS, FETCH_USERS_ERROR, SEARCH_USER } from "../actions/todoActions";


const initialUserInfo = {
    users: [],
    userInfo: {},
    loading: false,
    error: '',
    searchValue: ''
};

export const userReducer = (state = initialUserInfo, action) => {
    switch (action.type) {
        case FETCH_USER_BEGIN:
            return {
                ...state,
                loading: true
            }

        case FETCH_USER_SUCCESS:
            return {
                ...state,
                userInfo: action.userInfo,
                loading: false,
                error: ''
            }

        case FETCH_USER_ERROR:
            return {
                ...state,
                loading: false,
                error: action.error
            }

        case FETCH_USERS_BEGIN:
            return {
                ...state,
                loading: true
            }

        case FETCH_USERS_SUCCESS:
            return {
                ...state,
                users: action.users,
                loading: false,
                error: ''
            }

        case FETCH_USERS_ERROR:
            return {
                ...state,
                loading: false,
                error: action.error
            }

        case SEARCH_USER:
            return {
                ...state,
                searchValue: action.userName
            }

        default:
            return state
    }
}