import { combineReducers } from 'redux';
import { todoReducer } from './todoReducer';
import { userReducer } from './userReducer';
import { authReducer } from './authReducer';

export const rootReducer = combineReducers({
    todoList: todoReducer,
    users: userReducer,
    authService: authReducer
})
