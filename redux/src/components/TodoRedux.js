import React, { useEffect } from 'react';
import TodoForm from './TodoForm';
import TodoList from './TodoList';
import { useDispatch } from 'react-redux';
import { fetchTodos } from '../actions/todoActions';

const TodoRedux = () => {
    const dispatch = useDispatch();

    useEffect(() => {
        // dispatch(fetchTodos());
    }, []);

    return (
        <div>
            <h1>Todo Redux</h1>
            <TodoForm/>
            <TodoList />
        </div>
    )
}

export default TodoRedux;