import React from 'react';
import { useSelector } from 'react-redux';

const TodoList = () => {

    const todos = useSelector(state => state.todoCompState.data);

    return (
        <div>
            
            {todos.map((todo, index) =>
                <h4 key={index}>{todo.title}</h4>
            )}
        </div>
    )
}

export default TodoList;   