import React, { useState } from 'react';
import { useSelector, useDispatch, connect } from 'react-redux';
import { decrement, increment } from '../actions/couterActions';
import { bindActionCreators } from 'redux';

const CouterRedux = (props) => {

    const { counter, increment, decrement } = props;

    return (
        <div>
            <div>Couter Redux</div>
            <div>Counts = {counter}</div>
            <div>
                <button onClick={() => decrement(5)}>-</button>
                <button onClick={() => increment(5)}>+</button>
            </div>
        </div>
    )
}

const mapStateToprops = (state) => {
    return {
        counter: state.counter
    }
}

const mapDispatchToprops = (dispatch) => {
    return bindActionCreators({ increment, decrement }, dispatch)
}

export default connect(mapStateToprops, mapDispatchToprops)(CouterRedux);