import React, { useState } from 'react';

const Couter = () => {

    const [counts, setCounts] = useState(0);
    return (
        <div>
            <div>Counts = {counts}</div>
            <div>
                <button onClick={() => setCounts(counts-1)}>-</button>
                <button onClick={() => setCounts(counts+1)}>+</button>
            </div>
        </div>
    )
}

export default Couter;