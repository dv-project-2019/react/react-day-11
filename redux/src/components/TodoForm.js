import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { addTodo } from '../actions/todoActions';
import { Input } from 'antd';
import { Select } from 'antd';

const TodoForm = () => {
    const { Search } = Input;
    const { Option } = Select;

    const [newTodoName, setNewTodoName] = useState('');

    const dispatch = useDispatch();

    const clickedAddHandler = () => {
        dispatch(addTodo(newTodoName));
        setNewTodoName('');
    }

    const handleChange = (value) => {
        console.log(`selected ${value}`);
    }

    return (
        <div>
            <Search
                placeholder="input search text"
                onChange={(e) => setNewTodoName(e.target.value)}
                onSearch={clickedAddHandler}
                style={{ width: 200 }}
            />
            <Select defaultValue="lucy" style={{ width: 120 }} onChange={handleChange}>
                <Option value="jack">Jack</Option>
                <Option value="lucy">Lucy</Option>
                <Option value="Yiminghe">yiminghe</Option>
            </Select>
        </div>
    )
}

export default TodoForm;