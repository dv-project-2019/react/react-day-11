import React from 'react';
import logo from './logo.svg';
import './App.css';
import Couter from './components/Couter';
import CouterRedux from './components/CouterRedux';
import TodoRedux from './components/TodoRedux';

function App() {
  return (
    <div className="App">
      <Couter/>
      <CouterRedux/>
      <TodoRedux/>
    </div>
  );
}

export default App;
