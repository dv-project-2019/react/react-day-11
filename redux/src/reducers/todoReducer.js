import { ADD_TODO, FETCH_TODO_BEGIN, FETCH_TODO_SUCCESS, FETCH_TODO_ERROR } from "../actions/todoActions";

const initialState = {
    todos: [],
    loading: false,
    error: '',
    data: [
        {
            "userId": 1,
            "id": 1,
            "title": "delectus aut autem",
            "completed": false
        },
        {
            "userId": 1,
            "id": 2,
            "title": "quis ut nam facilis et officia qui",
            "completed": false
        },
        {
            "userId": 1,
            "id": 3,
            "title": "fugiat veniam minus",
            "completed": false
        },
        {
            "userId": 1,
            "id": 4,
            "title": "et porro tempora",
            "completed": true
        },
        {
            "userId": 1,
            "id": 5,
            "title": "laboriosam mollitia et enim quasi adipisci quia provident illum",
            "completed": false
        },
        {
            "userId": 1,
            "id": 6,
            "title": "qui ullam ratione quibusdam voluptatem quia omnis",
            "completed": false
        },
        {
            "userId": 1,
            "id": 7,
            "title": "illo expedita consequatur quia in",
            "completed": false
        },
        {
            "userId": 1,
            "id": 8,
            "title": "quo adipisci enim quam ut ab",
            "completed": true
        },
        {
            "userId": 1,
            "id": 9,
            "title": "molestiae perspiciatis ipsa",
            "completed": false
        },
        {
            "userId": 1,
            "id": 10,
            "title": "illo est ratione doloremque quia maiores aut",
            "completed": true
        }
    ]

};

export const todoReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TODO:
            const newTodo = {
                taskName: action.payLoad,
                completed: false
            }
            return [newTodo, ...state];

        case FETCH_TODO_BEGIN:
            return {
                ...state,
                loading: true
            };
        case FETCH_TODO_SUCCESS:
            return {
                todos: action.payLoad,
                loading: false,
                error: ''
            };

        case FETCH_TODO_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payLoad
            };

        default:
            return state
    }
}