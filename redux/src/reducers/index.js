import { combineReducers } from 'redux';
import { couterReducer } from './couterReducer';
import { todoReducer } from './todoReducer';

export const rootReducer = combineReducers({
    counter :couterReducer,
    todoCompState: todoReducer
})
