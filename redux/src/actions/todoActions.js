export const ADD_TODO = 'ADD_TODO';
export const FETCH_TODO_BEGIN = 'FETCH_TODO_BEGIN';
export const FETCH_TODO_SUCCESS = 'FETCH_TODO_SUCCESS';
export const FETCH_TODO_ERROR = 'FETCH_TODO_ERROR';

export const addTodo = (taskName) => {
    return {
        type: ADD_TODO,
        payLoad: taskName
    }

}

export const fetchTodos = () => {
    return dispatch => {
        dispatch(fetchTodoBegin())
        return fetch('https://jsonplaceholder.typicode.com/todos')
            .then(res => res.json())
            .then(data => {
                dispatch(fetchTodoSuccess(data))
            })
            .catch(error => dispatch(fetchTodoError(error)));
    }
}

export const fetchTodoBegin = () => {
    return {
        type: FETCH_TODO_BEGIN
    }

}

export const fetchTodoSuccess = (todos) => {
    return {
        type: FETCH_TODO_SUCCESS,
        payLoad: todos
    }
}

export const fetchTodoError = (error) => {
    return {
        type: FETCH_TODO_ERROR,
        payLoad: error
    }
}