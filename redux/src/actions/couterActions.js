export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';

export const increment = (value) => {
    const action = {
        type: INCREMENT,
        payLoad: value
    }
    return action;
}

export const decrement = (value) => {
    const action = {
        type: DECREMENT,
        payLoad: value
    }
    return action;
}
